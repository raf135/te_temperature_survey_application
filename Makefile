TARGET   = temperaturesurvey
TARGETFROMLIB   = temperaturesurveyfromlib
CC       = g++ -O2 -g
# compiling flags here
CFLAGS   = -Wall -I . -lboost_system -lboost_thread -lyaml-cpp -lpthread -ldl  -DBOOST_STACKTRACE_USE_BACKTRACE 
LINKER   = g++ -O2 -g -o 
CVERSION = -std=c++17
# linking flags here
LFLAGS   = -Wall -I . -lboost_system -lboost_thread -lyaml-cpp -lpthread -ldl -lbacktrace -DBOOST_STACKTRACE_USE_BACKTRACE 
INCDIR   = inc
SRCDIR   = src
OBJDIR   = obj
BINDIR   = bin
LIBDIR = lib

MKDIR = mkdir

MAIN:= main.cc
SOURCES  := $(wildcard $(SRCDIR)/*.cc)
#INCLUDES := $(wildcard $(INCDIR)/*.h)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.cc=$(OBJDIR)/%.o) #assign to each .cc file an .o file
rm       = rm -f
rmrec       = rm -r


all: makedirs $(BINDIR)/$(TARGET) group_to_libs

makedirs:
ifneq ("$(wildcard $(BINDIR))","")
	@echo "binary folder exists"
else
	$(MKDIR) $(BINDIR)
endif
ifneq ("$(wildcard $(OBJDIR))","")
	@echo "object folder exists"
else
	$(MKDIR) $(OBJDIR)
endif
ifneq ("$(wildcard $(LIBDIR))","")
	@echo "libraries folder exists"
else
	$(MKDIR) $(LIBDIR)
endif

$(BINDIR)/$(TARGET): $(OBJECTS) $(MAIN)
	@echo "Starting compiling binary"
	@$(LINKER) $@  $(CVERSION) $(OBJECTS) $(MAIN) $(LFLAGS)
	@echo "Linking complete!"

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cc $(INCDIR)/%.h
	@$(CC) $(CFLAGS) -c $(CVERSION) $< -o $@
	@echo "Compiled "$<" successfully!"

group_to_libs:
	@echo "Grouping objects to libraries"
	ar rcs $(LIBDIR)/libtmod.a $(OBJDIR)/tmod.o
	ar rcs $(LIBDIR)/libsup.a $(OBJDIR)/supervision.o $(OBJDIR)/request.o $(OBJDIR)/interface.o
	ar rcs $(LIBDIR)/libintimp.a  $(OBJDIR)/interfaceImpl.o $(OBJDIR)/sensor.o 
	@echo "Libraries complete!"
	
binary_from_libs: $(MAIN)
	@echo "Starting compiling binary with libs"  
	@$(LINKER) $(BINDIR)/$(TARGETFROMLIB)  $(CVERSION) $(MAIN) -L$(LIBDIR)/ -lintimp -lsup -ltmod $(LFLAGS)  
	@echo "Linking complete!"

.PHONY: clean $(LFLAGS)
clean:
	@$(rm) $(OBJECTS)
	@echo "Obj cleanup complete!"

.PHONY: remove
remove: clean
	@$(rm) $(BINDIR)/$(TARGET)
	@echo "Executable removed!"
	@$(rmrec) $(OBJDIR)
	@$(rmrec) $(BINDIR)
	@$(rmrec) $(LIBDIR)
	@echo "Binary & executable folders emptied & removed"
