/*
 * interface.h
 *
 *  Created on: Feb 5, 2021
 *      Author: rkrawczy
 */

#ifndef INC_INTERFACE_H_
#define INC_INTERFACE_H_

#include "tmod.h"
#include <cstdint>
#include <iostream>
#include <list>
#include "sensor.h"
#include <sstream>

namespace TempSurvey
{

	///\brief Exception class thrown when the hwadress is above max ADC range
	class OutOfRangeException : public std::exception
	{
		public:
		//methods
			///\brief Produce exception information
			virtual const char* what() const throw();
	};

	///\brief Exception class thrown when trying to add existing interface
	class SensorExistsException : public std::exception
	{
		public:
		//methods
			///\brief Produce exception information
			virtual const char* what() const throw();
	};

	///\brief Exception class thrown typically when trying to delete or modify nonexistemt sensor
	class NoSuchSensorException : public std::exception
	{
		public:
		//methods
			///\brief Produce exception information
			virtual const char* what() const throw();
	};

	///\brief exception class thrown typically when string stream pointer is a nullptr - happens typically when uninitialized or deleted externally
	class NoSstreamException : public std::exception
	{
		public:
		//methods
			///\brief Produce exception information
			virtual const char* what() const throw();
	};

	///\brief Class to interact with VME rack
	class Interface
	{
		public:
		//methods
			///\brief Constructor
			Interface();
			///\brief Add sensor given a hardware address and signal type
			virtual void addSensor(uint64_t hwAddr, TMod::sigType type)=0;
			///\brief Remove sensor given a hardware address
			virtual void removeSensor(uint64_t hwAddr)=0;
			///\brief Set sensor scaling data given existing hardware address, offset and scaling factor
			virtual void setScalingData(uint64_t hwAddr, float scalingFactor, float offset)=0;
			///\brief Produce list of sensors with their signal type and scaling data
			virtual void getSensorsList()=0;
			///\brief Assign stringstream where interface will output data
			virtual void setSstream(std::stringstream &stream)=0;
			///\brief Trigger temperature reads on the sensors and output report from the sensors
			virtual void triggerReadReport()=0;

		private:

		//variables
			std::stringstream *stream;

	};

}

#endif /* INC_INTERFACE_H_ */
