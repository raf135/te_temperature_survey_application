/*
 * supervision.h
 *
 *  Created on: Feb 5, 2021
 *      Author: rkrawczy
 */

#ifndef INC_SUPERVISION_H_
#define INC_SUPERVISION_H_

#include "interface.h"
#include "request.h"
#include <mutex>
#include <thread>
#include <queue>
#include <chrono>

namespace TempSurvey
{
	const double TRIGGER_PERIOD_S = 60.0;


	///\brief Class to supervise and interact with an interface
	class Supervision
	{
		public:
		//methods
			Supervision();
			//~Supervision();
			///\brief Set interface implementation
			void setInterfaceImp(Interface &interface);
			///\brief Start supervision thread
			void start();
			///\brief Stop supervision thread
			void stop();
			//Interface & getInterface();
			///\brief Set stringstream which is passed to interface to produce data
			void setSstream(std::stringstream &stream);
			///\brief Add sensor wrapper for the RequestAddSensor request
			void addSensor(uint64_t hwAddr, TMod::sigType type);
			///\brief Remove sensor wrapper for the RequestRemoveSensor request
			void removeSensor(uint64_t hwAddr);
			///\brief Set Sensor scaling data wrapper for the RequestSetScalingData request
			void setScalingData(uint64_t hwAddr, float scalingFactor, float offset);
			///\brief Get sensor list wrapper for the RequestGetSensorsList request
			void getSensorsList();
			///\brief Trigger temperature read and wrapper for the RequestTriggerReadTempReport request
			void triggerTemperatureRead();

		private:
		//methods
			//add new dymanically allocated request
			void addRequest(Request * request);
			//test if 60 seconds elapsed and if so trigger temperature read
			void serviceTimeTrigger();
			//main thread of supervision class
			void supervisionThread();
			//service all pending reqyests in the reqyests queue and remove them from the queue
			void serviceAllRequests();
			//service a request
			void serviceRequest(Request & request);
		//variables
			//pointer to the implemented VME interface
			Interface *interfacePtr;
			//to implement critical section for updating requests
			std::mutex requestsMutex;
			// bool for controlled ending of thread
			bool running=true;
			// thread pointer, kept to join on its ending
			std::thread *supervisionThreadPtr;
			//FIFO of pending user requests
			std::queue<Request*> requests;
			//variables for timing - moment of thread global start
			std::chrono::time_point<std::chrono::system_clock> global_start;
			//variables for timing - current minute of thread run
			std::chrono::time_point<std::chrono::system_clock> present_quantum;
			//output stringstream
			std::stringstream *stream;
	};
}

#endif /* INC_SUPERVISION_H_ */
