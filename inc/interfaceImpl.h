/*
 * interface.h
 *
 *  Created on: Feb 5, 2021
 *      Author: rkrawczy
 */

#ifndef INC_INTERFACE_IMPL_H_
#define INC_INTERFACE_IMPL_H_

#include "tmod.h"
#include <cstdint>
#include <iostream>
#include <list>
#include "sensor.h"
#include <sstream>
#include "interface.h"

namespace TempSurvey
{


	///\brief Class to interact with VME rack
	class InterfaceImpl : public Interface
	{
		public:
		//methods
			///\brief Constructor
			InterfaceImpl();
			///\brief Add sensor given a hardware address and signal type
			virtual void addSensor(uint64_t hwAddr, TMod::sigType type);
			///\brief Remove sensor given a hardware address
			virtual void removeSensor(uint64_t hwAddr);
			///\brief Set sensor scaling data given existing hardware address, offset and scaling factor
			virtual void setScalingData(uint64_t hwAddr, float scalingFactor, float offset);
			///\brief Produce list of sensors with their signal type and scaling data
			virtual void getSensorsList();
			///\brief Assign stringstream where interface will output data
			virtual void setSstream(std::stringstream &stream);
			///\brief Trigger temperature reads on the sensors and output report from the sensors
			virtual void triggerReadReport();

		private:
		//methods
			//find sensor in the sensors list given an hardware address
			std::list<Sensor>::iterator findSensorByHwAddr(uint16_t hwAddr);
			//Produce YAML report on temperatures of the sensors
			std::string getYAMLTemperature();
			//Produce YAML report the sensors
			std::string getYAMLSensors();
			//produce YAML timestamp info upon the readout
			std::string getYAMLReadoutTimestamp();
			//Trigger temperature reads on sensors
			void triggerRead();
		//variables
			//sensors list
			std::list<Sensor> registeredSensorsList;
			//string stream to output data
			std::stringstream *stream;

	};

}

#endif /* INC_INTERFACE_H_ */
