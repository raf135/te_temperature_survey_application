/*
 * tmod.h
 *
 *  Created on: Feb 5, 2021
 *      Author: rkrawczy
 */

#ifndef INC_TMOD_H_
#define INC_TMOD_H_

#include <cstdint>

namespace TMod
{
	//type of signals
	enum sigType { v_0_10, mA_4_20};

	//read ADCs temperatures
	int16_t tmodReadAdcs(uint16_t hardwareAddress);
	//get maximal number of ADCs in that VME rack
	uint16_t tmodMaxAdcs(void);

}

#endif /* INC_TMOD_H_ */
