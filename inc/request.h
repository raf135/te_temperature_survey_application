/*
 * request.h
 *
 *  Created on: Feb 7, 2021
 *      Author: rkrawczy
 */

#ifndef INC_REQUEST_H_
#define INC_REQUEST_H_

#include "interface.h"
#include "tmod.h"
#include <exception>

namespace TempSurvey
{


	///\brief  Base request class for thread-safe interface access
	class Request
	{
		public:
		//methods
			///\brief  Virtual destructor to correctly delete inherited classes
			virtual ~Request();
			///\brief  Virtual base class for interaction with the Interface commands
			virtual void execute(Interface &interface);
	};

	///\brief Child class of request to call interface to add sensor
	class RequestAddSensor : public Request
	{
		public:
		//methods
			///\brief Class constructor
			RequestAddSensor(uint64_t hwAddr, TMod::sigType type);
			///\brief  Virtual destructor to correctly delete inherited classes
			virtual ~RequestAddSensor();
			///\brief  Execute addSensor method of the Interface
			virtual void execute(Interface &interface);

		private:
		//variables
			uint64_t hwAddr;
			TMod::sigType type;
	};

	///\brief Child class of request to call interface to remove sensor
	class RequestRemoveSensor : public Request
	{
		public:
		//methods
			///\brief Class constructor
			RequestRemoveSensor(uint64_t hwAddr);
			///\brief  Virtual destructor to correctly delete inherited classes
			virtual ~RequestRemoveSensor();
			///\brief  Execute removeSensor method of the Interface
			virtual void execute(Interface &interface);

		private:
		//variables
			uint64_t hwAddr;
	};

	///\brief Child class of request to call interface to set sensor scaling data
	class RequestSetScalingData : public Request
	{
		public:
		//methods
			///\brief Class constructor
			RequestSetScalingData(uint64_t hwAddr, float scalingFactor, float offset);
			///\brief  Virtual destructor to correctly delete inherited classes
			virtual ~RequestSetScalingData();
			///\brief  Execute setScalingData method of the Interface
			virtual void execute(Interface &interface);

		private:
		//variables
			uint64_t hwAddr;
			float scalingFactor;
			float offset;

	};

	///\brief Child class of request to call interface to get sensors list
	class RequestGetSensorsList : public Request
	{
		public:
		//methods
			///\brief Class constructor
			RequestGetSensorsList();
			///\brief  Virtual destructor to correctly delete inherited classes
			virtual ~RequestGetSensorsList();
			///\brief  Execute getSensorsList method of the Interface
			virtual void execute(Interface &interface);
	};

	///\brief Child class of request to call interface to trigger temperature read and generate report
	class RequestTriggerReadTempReport : public Request
	{
		public:
		//methods
			///\brief Class constructor
			RequestTriggerReadTempReport();
			///\brief  Virtual destructor to correctly delete inherited classes
			virtual ~RequestTriggerReadTempReport();
			///\brief  Execute triggerReadReport method of the Interface
			virtual void execute(Interface &interface);
	};

	/*
	class RequestSetSstream : public Request
	{
		public:
			virtual ~RequestSetSstream();
			RequestSetSstream(std::stringstream &stream);
			virtual void execute(Supervision &supervision);
		private:
			std::stringstream &stream;
	};
	*/

};


#endif /* INC_REQUEST_H_ */
