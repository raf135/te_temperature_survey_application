/*
 * sensor.h
 *
 *  Created on: Feb 5, 2021
 *      Author: rkrawczy
 */

#ifndef INC_SENSOR_H_
#define INC_SENSOR_H_


#include <cstdint>
#include "tmod.h"
#include <limits>
#include <string>

namespace TempSurvey
{
	///\brief Class to hold and access basic sensor informations via the Interface class
	class Sensor
	{
		public:
		//methods
			///\brief Constructor with hardware address and signal type params
			Sensor(uint16_t hwAddr, TMod::sigType signalType);
			///\brief Update sensor's temperture
			void updateTemp();
			///\brief Get sensor's current temperture
			float getTemp();
			///\brief Get sensor's min temperture
			float getTempMin();
			///\brief Get sensor's max temperture
			float getTempMax();
			///\brief Set sensor's scaling factor
			void setScalingFactor(float scalingFactor);
			///\brief Get current sensor's scaling factor
			float getScalingFactor();
			///\brief Set sensor's scaling factor
			void setOffset(float offset);
			///\brief Get current sensor's Offset
			float getOffset();
			///\brief Get sensor's shardware adress
			uint16_t getAddr();
			///\brief Get sensor's signal type as string
			std::string getTypeString();

		private:
		//methods
			//normalise ADC-read temperature by scaling factor and offset
			float normaliseTemp(int16_t temp_int);

		//variables
			//hardware address
			uint16_t hwAddr;
			//type of signal  - mA or V
			TMod::sigType signalType;
			//Offset value
			float offset=0.0f;
			//scaling factor
			float scalingFactor=1.0f;
			//current temp after conversion with offset and scaling factor
			float currentTemp;
			//min normalised recorded temperature
			float minTemp=std::numeric_limits<float>::max();
			//max normalised recorded temperature
			float maxTemp=std::numeric_limits<float>::min();



	};
}

#endif /* INC_SENSOR_H_ */
