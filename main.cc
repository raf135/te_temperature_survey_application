/*
 * main.cc
 *
 *  Created on: Feb 3, 2021
 *      Author: rkrawczy
 */

#include "inc/supervision.h"
#include "inc/interfaceImpl.h"
#include <unistd.h>
#include <inc/request.h>
#include <iostream>

int main(void)
{

	TempSurvey::Supervision *supervision = new TempSurvey::Supervision();
    std::stringstream sstream;
    supervision->setSstream(sstream);

	TempSurvey::InterfaceImpl specificInterfaceImp;
    supervision->setInterfaceImp(specificInterfaceImp);

    std::cout<<"MAIN->STARTING SUPERVISION THREAD"<<std::endl;
    supervision->start();
    sleep(3);



    std::cout<<"MAIN->ADDING SENSOR"<<std::endl;
    supervision->addSensor(1,TMod::mA_4_20);
    supervision->addSensor(2,TMod::mA_4_20);
    supervision->addSensor(3,TMod::mA_4_20);
    sleep(3);

    std::cout<<"MAIN->MODIFYING SENSOR"<<std::endl;
    supervision->setScalingData(1, 1.0, 1.0);
    sleep(3);

    std::cout<<"MAIN->TRYING TO MODIFY NONEXISTENT SENSOR"<<std::endl;
    supervision->setScalingData(5, 1.0, 1.0);
    sleep(3);

    std::cout<<"MAIN->REQUESTING SENSORS LIST"<<std::endl;
    supervision->getSensorsList();
    sleep(3);

    std::cout<<"MAIN->REQUESTING TEMPERATURE READ TRIGGER & REPORT"<<std::endl;
    supervision->triggerTemperatureRead();
    sleep(3);

    std::cout<<"MAIN->ADDING SENSOR WITH TOO HIGH HWADDR"<<std::endl;
    supervision->addSensor(3000,TMod::mA_4_20);
    sleep(3);

    std::cout<<"MAIN->ADDING SENSOR THAT ALREADY EXISTS"<<std::endl;
    supervision->addSensor(3,TMod::mA_4_20);
    sleep(3);

    std::cout<<"MAIN->REMOVING SENSOR"<<std::endl;
    supervision->removeSensor(3);
    sleep(3);

    std::cout<<"MAIN->REMOVING NONEXISTENT SENSOR"<<std::endl;
    supervision->removeSensor(3);

    sleep(130);

    std::cout<<"MAIN->STOPPING SUPERVISION THREAD"<<std::endl;
    supervision->stop();

    std::cout<<"MAIN->STRINGSTREAM AT THE END"<<std::endl;

    std::cout<<sstream.str()<<std::endl;

    std::cout<<"MAIN->QUITTING"<<std::endl;

}
