Temperature survey application

Author:
	Rafal Dominik Krawczyk, CERN

About:
	This is a simple demonstrator application of interface and a supervision class to comunicate with VME racks.
	The main funciton runs some basic interacion with the interface and supervision.

Compiling:
	make
	
To delete all compiled files:
	make remove

External dependencies:
	libyaml-cpp-dev

Verified & developed with: 

	Linux Ubuntu  20.04 LTS
	5.8.0-41-generic
	gcc version 9.3.0 

Running:
	./bin/temperaturesurvey
