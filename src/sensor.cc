/*
 * sensor.cc
 *
 *  Created on: Feb 5, 2021
 *      Author: rkrawczy
 */

#include "../inc/sensor.h"
#include "../inc/tmod.h"

namespace TempSurvey
{
	Sensor::Sensor(uint16_t hwAddr, TMod::sigType signalType)
	: hwAddr{hwAddr}, signalType{signalType}{};



	void Sensor::updateTemp()
	{
		int16_t temp_int = TMod::tmodReadAdcs(this->hwAddr);
		this->currentTemp=normaliseTemp(temp_int);
		if(this->maxTemp<this->currentTemp)
		{
			this->maxTemp=this->currentTemp;
		}
		if(this->minTemp>this->currentTemp)
		{
			this->minTemp=this->currentTemp;
		}
	};

	float Sensor::normaliseTemp(int16_t tempInt)
	{
		float temp_float=static_cast<float>(tempInt);
		temp_float*=this->scalingFactor;
		temp_float-=this->offset;
		return temp_float;
	};

	void Sensor::setScalingFactor(float scalingFactor)
	{
		this->scalingFactor=scalingFactor;
	};

	float Sensor::getScalingFactor()
	{
		return this->scalingFactor;
	};

	void Sensor::setOffset(float offset)
	{
		this->offset=offset;
	};

	float Sensor::getOffset()
	{
		return this->offset;
	};

	uint16_t Sensor::getAddr()
	{
		return this->hwAddr;
	};

	float Sensor::getTemp()
	{
	 return this->currentTemp;
	};

	float Sensor::getTempMin()
	{
	 return this->minTemp;
	};

	float Sensor::getTempMax()
	{
	 return this->maxTemp;
	};

	std::string Sensor::getTypeString()
	{
		if(this->signalType==TMod::v_0_10)
		{
			return "v_0_10";
		}
		if(this->signalType==TMod::mA_4_20)
		{
			return "mA_4_20";
		}
			return "ERROR";
	}

}

