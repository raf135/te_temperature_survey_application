/*
 * supervision.cpp
 *
 *  Created on: Feb 5, 2021
 *      Author: rkrawczy
 */

#include "../inc/supervision.h"
#include "../inc/tmod.h"
#include "yaml-cpp/yaml.h"

namespace TempSurvey
{
	Supervision::Supervision()
		: stream{nullptr}
	{
		this->global_start= std::chrono::system_clock::now();
	};
/*
	Supervision::~Supervision()
	{

	};
*/
	void Supervision::setInterfaceImp(Interface &interface)
	{
		this->interfacePtr=&(interface);
		this->interfacePtr->setSstream(*(this->stream));
	};

	void Supervision::start()
	{
		this->running=true;
		supervisionThreadPtr = new std::thread(&Supervision::supervisionThread,this);
	};

	void Supervision::stop()
	{
		this->running=false;
		this->supervisionThreadPtr->join();
	};

	void Supervision::supervisionThread()
	{
		//start timer
		this->present_quantum= std::chrono::system_clock::now();
		while(running==true)
		{
			serviceAllRequests();
			serviceTimeTrigger();
		}
	};

	void Supervision::serviceAllRequests()
	{
		requestsMutex.lock();
		while(!this->requests.empty())
		{
			//std::cout<<"SUPERVISION GOT REQUEST"<<std::endl;
			Request* request = this->requests.front();
			serviceRequest(*request);
			this->requests.pop();
		}
		requestsMutex.unlock();
	};


	void Supervision::serviceRequest(Request & request)
	{
		request.execute(*(this->interfacePtr));
	};

	/*
	Interface& Supervision::getInterface()
	{
		return *(this->interfacePtr);
	};
	*/

	void Supervision::addRequest(Request * request)
	{
		requestsMutex.lock();
		this->requests.push(request);
		requestsMutex.unlock();
	};

	void Supervision::serviceTimeTrigger()
	{
		auto now = std::chrono::system_clock::now();
		std::chrono::duration<double> diff = now-this->present_quantum;
		if(diff.count()>=TRIGGER_PERIOD_S)
		{
			std::cout<<"60 seconds elapsed"<<std::endl;
			this->present_quantum=now;
			this->addRequest(new TempSurvey::RequestTriggerReadTempReport());
		}
	};

	void Supervision::setSstream(std::stringstream &stream)
	{
		this->stream=&stream;
	};

	void Supervision::addSensor(uint64_t hwAddr, TMod::sigType type)
	{
		addRequest(new TempSurvey::RequestAddSensor(hwAddr,type));
	};

	void Supervision::removeSensor(uint64_t hwAddr)
	{
		addRequest(new TempSurvey::RequestRemoveSensor(hwAddr));
	};

	void Supervision::setScalingData(uint64_t hwAddr, float scalingFactor, float offset)
	{
		addRequest(new TempSurvey::RequestSetScalingData(hwAddr, scalingFactor, offset));
	};

	void Supervision::getSensorsList()
	{
		addRequest(new TempSurvey::RequestGetSensorsList());
	};

	void Supervision::triggerTemperatureRead()
	{
		addRequest(new TempSurvey::RequestTriggerReadTempReport());
	};



}
