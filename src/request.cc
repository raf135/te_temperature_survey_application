/*
 * request.cc
 *
 *  Created on: Feb 7, 2021
 *      Author: rkrawczy
 */

#include "../inc/request.h"
#include <boost/stacktrace.hpp>

namespace TempSurvey
{

	void Request::execute(Interface &interface)
	{
		//std::cout<<"REQUEST IN BASE EXECUTE"<<std::endl;
	};

	Request::~Request(){};


	RequestAddSensor::RequestAddSensor(uint64_t hwAddr, TMod::sigType type)
		: hwAddr{hwAddr}, type{type}{};

	RequestAddSensor::~RequestAddSensor(){};

	void RequestAddSensor::execute(Interface &interface)
	{
		//std::cout<<"REQUEST IN ADD SENSOR EXECUTE"<<std::endl;
		try
		{
			interface.addSensor(hwAddr,type);
		}
		catch (std::exception& e)
		{
		    std::cout << "Request encountered exception: "
		    		<< e.what() << " Stacktrace:"
					<< boost::stacktrace::stacktrace() << std::endl;
		}
	};


	RequestRemoveSensor::RequestRemoveSensor(uint64_t hwAddr)
		: hwAddr{hwAddr}{};

	RequestRemoveSensor::~RequestRemoveSensor(){};

	void RequestRemoveSensor::execute(Interface &interface)
	{
		//std::cout<<"REQUEST IN REMOVE SENSOR EXECUTE"<<std::endl;
		try
		{
			interface.removeSensor(hwAddr);
		}
		catch (std::exception& e)
		{
		    std::cout << "Request encountered exception: "
		    		<< e.what()<< " Stacktrace:"
					<< boost::stacktrace::stacktrace() << std::endl;
		}
	};


	RequestSetScalingData::RequestSetScalingData(uint64_t hwAddr, float scalingFactor, float offset)
		: hwAddr{hwAddr}, scalingFactor{scalingFactor}, offset{offset}{};

	RequestSetScalingData::~RequestSetScalingData(){};

	void RequestSetScalingData::execute(Interface &interface)
	{
		try
		{
			//std::cout<<"REQUEST IN SCALING EXECUTE"<<std::endl;
			interface.setScalingData(hwAddr, scalingFactor, offset);
		}
		catch (std::exception& e)
		{
			std::cout << "Request encountered exception: "
					<< e.what()<< " Stacktrace:"
					<< boost::stacktrace::stacktrace() << std::endl;
		}



	};


	RequestGetSensorsList::RequestGetSensorsList(){};

	RequestGetSensorsList::~RequestGetSensorsList(){};

	void RequestGetSensorsList::execute(Interface &interface)
	{
		try
		{
			//std::cout<<"REQUEST IN GET LIST EXECUTE"<<std::endl;
			interface.getSensorsList();
		}
		catch (std::exception& e)
		{
		    std::cout << "Request encountered exception: "
		    		<< e.what()<< " Stacktrace:"
					<< boost::stacktrace::stacktrace() << std::endl;
		}
	};

	RequestTriggerReadTempReport::RequestTriggerReadTempReport(){};

	RequestTriggerReadTempReport::~RequestTriggerReadTempReport(){};

	void RequestTriggerReadTempReport::execute(Interface &interface)
	{
		try
		{
			//std::cout<<"REQUEST IN READ TEMP EXECUTE"<<std::endl;
			interface.triggerReadReport();
		}
		catch (std::exception& e)
		{
		    std::cout << "Request encountered exception: "
		    		<< e.what()<< " Stacktrace:"
					<< boost::stacktrace::stacktrace() << std::endl;
		}
	};

	/*
	RequestSetSstream::RequestSetSstream(std::stringstream &stream)
		: stream(stream){};

	RequestSetSstream::~RequestSetSstream(){};

	void RequestSetSstream::execute(Supervision &supervision)
	{
		std::cout<<"REQUEST IN OSTREAM EXECUTE"<<std::endl;
		supervision.getInterface().setSstream(*(this->stream));
	};
	*/
}
