/*
 * interface.cpp
 *
 *  Created on: Feb 5, 2021
 *      Author: rkrawczy
 */

#include "../inc/tmod.h"
#include "../inc/interface.h"
#include "../inc/interfaceImpl.h"
#include "yaml-cpp/yaml.h"
#include <chrono>
#include <ctime>
#include <iostream>
#include <iomanip>

namespace TempSurvey
{

	InterfaceImpl::InterfaceImpl()
	{

	};

	void InterfaceImpl::addSensor(uint64_t hwAddr, TMod::sigType type)
	{
		//std::cout<<"INTERFACE: IN ADD SENSOR"<<std::endl;
		//first check if the given hardware address is correct
		uint16_t max_adcs = TMod::tmodMaxAdcs();
		if(hwAddr>max_adcs)
			throw OutOfRangeException();

		//check if isn't already there
		//if exists thenthrow an exception
		std::list<Sensor>::iterator i = findSensorByHwAddr(hwAddr);
		if(i!=this->registeredSensorsList.end())
		{
			throw SensorExistsException();
		}
		registeredSensorsList.push_back(Sensor(hwAddr,type));

	};

	void InterfaceImpl::removeSensor(uint64_t hwAddr)
	{
		//std::cout<<"INTERFACE: IN REMOVE SENSOR"<<std::endl;
		std::list<Sensor>::iterator i = findSensorByHwAddr(hwAddr);
		//if sensor doesn't exist then throw an exception
		if(i==this->registeredSensorsList.end())
		{
			throw NoSuchSensorException();
		}
		registeredSensorsList.erase(i);
	};

	void InterfaceImpl::setScalingData(uint64_t hwAddr, float scalingFactor, float offset)
	{
		std::list<Sensor>::iterator i = findSensorByHwAddr(hwAddr);

		//if sensor doesn't exist then throw an exception
		if(i==this->registeredSensorsList.end())
		{
			throw NoSuchSensorException();
		}
		i->setScalingFactor(scalingFactor);
		i->setOffset(offset);
	};

	void InterfaceImpl::setSstream(std::stringstream &stream)
	{
		this->stream=&stream;
	};

	void InterfaceImpl::triggerReadReport()
	{
		//first trigger update on the inrerface sensor structures
		triggerRead();
		//now produce the report
		getYAMLTemperature();
	};


	void InterfaceImpl::triggerRead()
	{
		for(auto &sensor:registeredSensorsList)
		{
			sensor.updateTemp();
		}
		std::string yamlString = getYAMLTemperature();
		//std::cout << yamlString;
		if(stream==nullptr)
		{
			throw NoSstreamException();
		}
		*(stream) << getYAMLReadoutTimestamp() << std::endl << std::endl;
		*(stream) << yamlString << std::endl << std::endl;
	};

	std::list<Sensor>::iterator InterfaceImpl::findSensorByHwAddr(uint16_t hwAddr)
	{
		for(std::list<Sensor>::iterator i = this->registeredSensorsList.begin(); i!=this->registeredSensorsList.end(); ++i)
		{
			if(i->getAddr()==hwAddr)
				return i;
		}
		return this->registeredSensorsList.end();
	};

	void InterfaceImpl::getSensorsList()
	{
		std::string yamlString = getYAMLSensors();
		//std::cout << yamlString;
		if(stream==nullptr)
		{
			throw NoSstreamException();
		}
		*(stream) << getYAMLReadoutTimestamp() << std::endl << std::endl;
		*(stream) << yamlString << std::endl << std::endl;
	};

	std::string InterfaceImpl::getYAMLSensors()
	{
		YAML::Emitter out;
		for (auto sensor :this->registeredSensorsList)
		{
			out << YAML::BeginMap;
			out << YAML::Key << "HwAddr";
			out << YAML::Value << sensor.getAddr();
			out << YAML::Key << "SignalType";
			out << YAML::Value << sensor.getTypeString();
			out << YAML::Key << "Offset:";
			out << YAML::Value <<  sensor.getOffset();
			out << YAML::Key << "ScalingFactor:";
			out << YAML::Value <<  sensor.getScalingFactor();
			out << YAML::EndMap;
		}
		return std::string(out.c_str());
	}

	std::string InterfaceImpl::getYAMLTemperature()
	{
		YAML::Emitter out;
		for (auto sensor : this->registeredSensorsList)
		{
			out << YAML::BeginMap;
			out << YAML::Key << "HwAddr";
			out << YAML::Value << sensor.getAddr();
			out << YAML::Key << "CurrentTemp:";
			out << YAML::Value <<  sensor.getTemp();
			out << YAML::Key << "MinTemp:";
			out << YAML::Value <<  sensor.getTempMin();
			out << YAML::Key << "MaxTemp:";
			out << YAML::Value <<  sensor.getTempMax();
			out << YAML::EndMap;
		}
		return std::string(out.c_str());
	};

	std::string  InterfaceImpl::getYAMLReadoutTimestamp()
	{

	    auto now = std::chrono::system_clock::now();
	    auto in_time_t = std::chrono::system_clock::to_time_t(now);
	    std::stringstream sStream;
	    sStream << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X");

		YAML::Emitter out;
		out << YAML::BeginMap;
		out << YAML::Key << "ReadoutTriggerTimestamp";
		out << YAML::Value <<sStream.str();
		out << YAML::EndMap;
		return std::string(out.c_str());
	};
}



