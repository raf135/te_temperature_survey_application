/*
 * interface.cpp
 *
 *  Created on: Feb 5, 2021
 *      Author: rkrawczy
 */

#include "../inc/interface.h"
#include "yaml-cpp/yaml.h"
#include <chrono>
#include <ctime>
#include <iostream>
#include <iomanip>

namespace TempSurvey
{

	const char* OutOfRangeException::what() const throw()
	{
		return "Hwaddr out of max range. ";
	};

	//exception thrown when the hwadress ia above max ADC range
	const char* SensorExistsException::what() const throw()
	{
		return "Sensor already exists. ";
	};

	const char* NoSuchSensorException::what() const throw()
	{
		return "No such sensor. ";
	};

	const char* NoSstreamException::what() const throw()
	{
		return "No output stringstream assigned. ";
	};

	Interface::Interface()
	: stream{nullptr}{};

}



